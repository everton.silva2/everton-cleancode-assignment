class Employee {
    name;
    workedHours;
    salary;
    
    constructor(name, workedHours) {
        this.name = name;
        this.workedHours = workedHours;
    }

    calculateSalary(shiftStrategy) {
        return new shiftStrategy().calculateSalary(this.workedHours);
    }
}

module.exports = Employee;