class AfternoonShiftStrategy extends Shift{
    constructor() {};
    
    additional = 50;
    salaryPerHour = 1.2;

    calculateAdditional() {
        return this.additional * this.salaryPerHour;
    }

    calculateSalary(workedHours) {
        return super.calculateWorkedHours(workedHours, this.salaryPerHour) + this.calculateAdditional();
    }
}

module.exports = AfternoonShiftStrategy;