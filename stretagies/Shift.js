class Shift {
    constructor() { };

    calculateWorkedHours(workedHours, salaryPerHour) {
        return workedHours * salaryPerHour;
    }
}