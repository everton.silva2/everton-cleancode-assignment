class NightShiftStrategy extends Shift{
    constructor() {};
    nightAdditional = 200;
    salaryPerHour = 2;
    
    
    calculateSalary(workedHours) {
        return super.calculateWorkedHours(workedHours, this.salaryPerHour) + this.nightAdditional;
    }
}

module.exports = NightShiftStrategy;