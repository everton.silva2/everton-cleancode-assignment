class MorningShiftStrategy extends Shift {
    constructor() {};

    salaryPerHour = 1;

    calculateSalary(workedHours) {
        return super.calculateWorkedHours(workedHours, this.salaryPerHour);
    }
}

module.exports = MorningShiftStrategy;