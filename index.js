const express = require('express');
const employeeRouter = require('./routes/employeeRouter.js');

const app = express();
const PORT = 3000;

app.listen(PORT, () => {
    console.log('Listening on port ' + PORT);
})

app.set('view engine', 'pug');
app.use(express.json());
app.use('/employee', employeeRouter);