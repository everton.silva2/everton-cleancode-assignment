class LocalDatabase {
    static instance;
    records = {};
    
    constructor(
        tableName
    ) {
        if(LocalDatabase.instance && LocalDatabase.tableNameInstance === tableName) {
            return LocalDatabase.instance;
        }

        this.tableName = tableName;
        Object.assign(this.records, { [tableName]: { data: [] }});

    };

    save(record) {
        this.records[this.tableName].data.push(record);
    }

    get() {
        return this.records[this.tableName].data;
    }
}

module.exports = LocalDatabase;