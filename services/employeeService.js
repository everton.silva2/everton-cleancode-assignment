class EmployeeService {
    databaseInstance;

    constructor(
        databaseInstance
    ) {
        this.databaseInstance = databaseInstance;
    };


    insertEmployee(employee) {
        this.databaseInstance.save(employee);
    }

    getEmployees() {
        return this.databaseInstance.get();
    }

}

module.exports = EmployeeService;