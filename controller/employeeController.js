const AfternoonShiftStrategy = require("../stretagies/AfternoonShiftStrategy");
const MorningShiftStrategy = require("../stretagies/MorningShiftStrategy");
const NightShiftStrategy = require("../stretagies/NightShiftStrategy");
const Employee = require("../models/Employee.js");

class EmployeeController {
    employeeService
    constructor(employeeService) {
        this.employeeService = employeeService
    };

    insertEmployee(request, response) {
        const { shiftTime, name, workedHours } = request.body;
        if(!shiftTime || !name || !workedHours) {
            response.status(400).send({
                message: 'An error has occured, required field is missing'
            });
        }

        try {
            const employee = new Employee(name, workedHours);
            const employeeShiftStrategy = this.getEmployeeShiftStrategy(shiftTime);
            employee.salary = employee.calculateSalary(employeeShiftStrategy);
    
            this.employeeService.insertEmployee(employee);
            response.status(201).send({
                message: 'Employee registered successfully'
            });
        } catch(err) {
            response.status(400).send({
                message: err.message
            });
        }
    }

    getEmployeeShiftStrategy(shiftTime) {
        switch(shiftTime) {
            case 'morning': 
                return MorningShiftStrategy;
            case 'afternoon':
                return AfternoonShiftStrategy;
            case 'night':
                return NightShiftStrategy;
            default:
                throw new Error('Unknown shift time.');
        }
    }

    getEmployees() {
        return this.employeeService.getEmployees();
    }
}

module.exports = EmployeeController;