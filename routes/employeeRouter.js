const router = require('express').Router();
const EmployeeController = require('../controller/employeeController.js');
const DatabaseInstance = require('../databases/localDatabase.js');
const EmployeeService = require('../services/employeeService.js');


const employeeController = new EmployeeController(
	new EmployeeService(new DatabaseInstance('employee'))
);

router.post('/', (request, response) => {
	employeeController.insertEmployee(request, response);
});


router.get('/list', (request, response) => {
	response.render('../view/employees/list.pug',
		{
			title: 'Employee List',
			employeesData: employeeController.getEmployees()
		}
	);
});

module.exports = router;